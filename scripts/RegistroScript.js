var app = angular.module('SiGECApp', ['ngMaterial']);
app.controller('RegCtrl', function($scope, $http) {
    $scope.types = [];
    $scope.apartments = 
        function getApartamentos() {
            var apart = [];
                for (i = 1; i < 30; i++) {
                    for (j = 1; j < 9; j++) {
                        var value = "" + i + "0" + j;
                        apart.push(value);
                    }
                }
                return apart;
    }();
    $scope.blocks = [];
	$scope.medidas = [];
    $scope.types.push("Carta", "Pacote", "Revista", "Jornal");
	$scope.medidas.push("kg", "g");
    $scope.blocks.push("Palmeiras", "Parque");
});

