(function(){

  angular
       .module('packages')
       .controller('PackageController', [
          'packageService', '$mdSidenav', '$mdBottomSheet', '$timeout', '$log',
          PackageController
       ]);

  /**
   * Main Controller for the Angular Material Starter App
   * @param $scope
   * @param $mdSidenav
   * @param avatarsService
   * @constructor
   */
    
  function PackageController( packageService, $mdSidenav, $mdBottomSheet, $timeout, $log ) {
    var self = this;

    self.selected     = null;
    self.packages        = [ ];
    self.selectPackage   = selectPackage;
    self.toggleList   = togglePackagesList;
    self.makeContact  = makeContact;

    // Load all registered packages

    packageService
          .loadAllPackages()
          .then( function( packages ) {
            self.packages    = [].concat(packages);
            self.selected = packages[0];
          });

    // *********************************
    // Internal methods
    // *********************************

    /**
     * Hide or Show the 'left' sideNav area
     */
    function togglePackagesList() {
      $mdSidenav('left').toggle();
    }

    /**
     * Select the current avatars
     * @param menuId
     */
    function selectPackage ( package ) {
      self.selected = angular.isNumber(package) ? $scope.packages[package] : package;
    }

    /**
     * Show the Contact view in the bottom sheet
     */
    function makeContact(selectedPackage) {

        $mdBottomSheet.show({
          controllerAs  : "vm",
          templateUrl   : './src/packages/view/contactSheet.html',
          controller    : [ '$mdBottomSheet', ContactSheetController],
          parent        : angular.element(document.getElementById('content'))
        }).then(function(clickedItem) {
          $log.debug( clickedItem.name + ' clicked!');
        });

        /**
         * User ContactSheet controller
         */
        function ContactSheetController( $mdBottomSheet ) {
          this.package = selectedPackage;
          this.items = [
            { name: 'Phone'       , icon: 'phone'       , icon_url: 'assets/svg/phone.svg'},
            { name: 'Twitter'     , icon: 'twitter'     , icon_url: 'assets/svg/twitter.svg'},
            { name: 'Google+'     , icon: 'google_plus' , icon_url: 'assets/svg/google_plus.svg'},
            { name: 'Hangout'     , icon: 'hangouts'    , icon_url: 'assets/svg/hangouts.svg'}
          ];
          this.contactUser = function(action) {
            // The actually contact process has not been implemented...
            // so just hide the bottomSheet

            $mdBottomSheet.hide(action);
          };
        }
    }

  }

})();
