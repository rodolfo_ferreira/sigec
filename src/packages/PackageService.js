(function(){
  'use strict';

  angular.module('packages')
         .service('packageService', ['$q', PackageService]);

  /**
   * Users DataService
   * Uses embedded, hard-coded data model; acts asynchronously to simulate
   * remote data service call(s).
   *
   * @returns {{loadAll: Function}}
   * @constructor
   */
  function PackageService($q){
    var packages = [
      {
        enterprise: 'Dell',
        date: "19/05/2016",
        type: 'Pacote',
        peso: '1,3',
        unidade: 'kg',
        name: 'Rodolfo André Barbosa Ferreira',
        bloco: 'Parque',
        apartamento: '2004',
        description: 'Pacote cinza grande',
        status:  'Entregue',
        receptor: 'Severino',
        image: 'assets/svg/encomenda.jpg'
      },
      {
        enterprise: 'Época',
        date: "19/05/2016",
        type: 'Revista',
        peso: '200',
        unidade: 'g',
        name: 'Rodolfo André Barbosa Ferreira',
        bloco: 'Parque',
        apartamento: '2004',
        description: 'Revista capa Michael Temer Poeta',
        status: 'Entregue',
        receptor: 'Severino',
        image: 'assets/svg/magazine-icon.png',
      },
      {
        enterprise: 'Jornal do Comércio',
        date: "18/05/2016",
        type: 'Jornal',
        peso: '400',
        unidade: 'g',
        name: 'Rodolfo André Barbosa Ferreira',
        bloco: 'Parque',
        apartamento: '2004',
        description: 'Capa sobre avião desaparecido',
        status: 'Não entregue',
        receptor: 'Severino',
        image: 'assets/svg/web-news-icon.jpg'
      },
      {
        enterprise: 'Correios',
        date: "17/05/2016",
        type: 'Carta',
        peso: '50',
        unidade: 'g',
        name: 'Rodolfo André Barbosa Ferreira',
        bloco: 'Parque',
        apartamento: '2004',
        description: 'Carta de mala direta',
        status: 'Entregue',
        receptor: 'Severino',  
        image: 'assets/svg/encomenda.jpg'
      },
      {
        enterprise: 'Não Identificado',
        date: "17/05/2016",
        type: 'Pacote',
        peso: '2',
        unidade: 'kg',
        name: 'Rodolfo André Barbosa Ferreira',
        bloco: 'Parque',
        apartamento: '2004',
        description: 'Pacote cinza',
        status: 'Não entregue',
        receptor: 'Severino',
        image: 'assets/svg/encomenda.jpg'
      },
      {
        enterprise: 'DirectLog',
        date: "15/05/2016",
        type: 'Pacote',
        peso: '900',
        unidade: 'g',
        name: 'Rodolfo André Barbosa Ferreira',
        bloco: 'Parque',
        apartamento: '2004',
        description: 'Pacote da Americanas.com',
        status: 'Entregue',
        receptor: 'Severino',  
        image: 'assets/svg/encomenda.jpg'
      }
    ];

    // Promise-based API
    return {
      loadAllPackages : function() {
        // Simulate async nature of real remote calls
        return $q.when(packages);
      }
    };
  }

})();
